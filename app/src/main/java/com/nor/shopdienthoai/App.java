package com.nor.shopdienthoai;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.nor.shopdienthoai.models.Cart;
import com.nor.shopdienthoai.models.CartDetail;
import com.nor.shopdienthoai.models.User;

import java.util.ArrayList;

public class App extends Application {

    private static App instance;

    private MutableLiveData<Cart> carts = new MutableLiveData<>();
    private User user;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        carts.postValue(new Cart());
    }

    public static App getInstance() {
        return instance;
    }

    public MutableLiveData<Cart> getCarts() {
        return carts;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
