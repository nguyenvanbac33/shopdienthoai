package com.nor.shopdienthoai.ui.item;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nor.shopdienthoai.R;

import java.util.ArrayList;

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.ColorHolder>{

    private ArrayList<String> data;
    private LayoutInflater inflater;

    public ColorAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ColorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_color, parent, false);
        return new ColorHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorHolder holder, final int position) {
        holder.bindData(data.get(position));
        holder.imDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.remove(position);
                notifyItemRangeRemoved(position, 1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class ColorHolder extends RecyclerView.ViewHolder {

        private TextView tvColor;
        private ImageView imDelete;

        public ColorHolder(@NonNull View itemView) {
            super(itemView);
            tvColor = itemView.findViewById(R.id.tv_color);
            imDelete = itemView.findViewById(R.id.im_delete);
        }

        public void bindData(String color) {
            tvColor.setText(color);
        }
    }
}
