package com.nor.shopdienthoai.ui.profile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.nor.shopdienthoai.App;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.FragmentBase;
import com.nor.shopdienthoai.models.User;
import com.nor.shopdienthoai.ui.account.LoginActivity;
import com.nor.shopdienthoai.utils.DialogUtls;
import com.nor.shopdienthoai.utils.ShareDataUtls;
import com.nor.shopdienthoai.utils.StorageUtils;
import com.nor.shopdienthoai.utils.Validators;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends FragmentBase implements View.OnClickListener, OnSuccessListener<Void>, OnFailureListener, StorageUtils.UploadCallback {

    private static final int REQUEST_IMAGE = 1;
    private ImageView imAvatar;
    private TextView tvEmail;
    private EditText edtName;
    private EditText edtAddress;
    private EditText edtPhone;
    private EditText edtPassword;
    private Button btnUpdate;
    private Button btnLogout;
    private Button btnLogin;
    private LinearLayout lnInfo;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_account;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        imAvatar = findViewById(R.id.im_avatar);
        tvEmail = findViewById(R.id.tv_email);
        edtAddress = findViewById(R.id.edt_address);
        edtName = findViewById(R.id.edt_name);
        edtPassword = findViewById(R.id.edt_password);
        edtPhone = findViewById(R.id.edt_sdt);
        btnUpdate = findViewById(R.id.btn_update);
        btnLogout = findViewById(R.id.btn_logout);

        lnInfo = findViewById(R.id.ln_info);
        btnLogin = findViewById(R.id.btn_login);

        imAvatar.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        btnLogin.setOnClickListener(this);

        if (App.getInstance().getUser() == null) {
            btnLogin.setVisibility(View.VISIBLE);
            lnInfo.setVisibility(View.GONE);
        }else {
            lnInfo.setVisibility(View.VISIBLE);
            btnLogin.setVisibility(View.GONE);
        }
        loadInfo();
    }

    private void loadInfo() {
        User user = App.getInstance().getUser();
        if (user == null) return;
        Glide.with(imAvatar).load(user.getAvatar()).placeholder(R.drawable.ic_person).error(R.drawable.ic_person)
                .into(imAvatar);
        tvEmail.setText(user.getEmail());
        edtPhone.setText(user.getPhone());
        edtPassword.setText(user.getPassword());
        edtName.setText(user.getName());
        edtAddress.setText(user.getAddress());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.im_avatar:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Pick image"), REQUEST_IMAGE);
                break;
            case R.id.btn_update:
                if (Validators.isEmpty(edtAddress, edtName, edtPassword, edtPhone)) return;
                DialogUtls.showProgressDialog(getContext());
                App.getInstance().getUser().setAddress(edtAddress.getText().toString());
                App.getInstance().getUser().setName(edtName.getText().toString());
                App.getInstance().getUser().setPassword(edtPassword.getText().toString());
                App.getInstance().getUser().setPhone(edtPhone.getText().toString());
                FirebaseAuth.getInstance().getCurrentUser().updatePassword(edtPassword.getText().toString());
                FirebaseDatabase.getInstance().getReference(App.getInstance().getUser().getRoot())
                        .child(App.getInstance().getUser().getId() + "").setValue(App.getInstance().getUser())
                        .addOnSuccessListener(this)
                        .addOnFailureListener(this);
                break;
            case R.id.btn_logout:
            case R.id.btn_login:
                ShareDataUtls.getInstance(getContext()).put(ShareDataUtls.Keys.EMAIL, "");
                ShareDataUtls.getInstance(getContext()).put(ShareDataUtls.Keys.PASSWORD, "");
                startActivity(new Intent(getContext(), LoginActivity.class));
                getActivity().finish();
                break;
        }
    }

    @Override
    public void onSuccess(Void aVoid) {
        DialogUtls.dismissProgress();
        Toast.makeText(getContext(), "Update success", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        DialogUtls.dismissProgress();
        Toast.makeText(getContext(), "Update fail", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            StorageUtils.uploadImage(getContext(), uri, this);
        }
    }

    @Override
    public void onUploadSuccess(String path) {
        App.getInstance().getUser().setAvatar(path);
        loadInfo();
    }
}
