package com.nor.shopdienthoai.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.nor.shopdienthoai.App;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.ActivityBase;
import com.nor.shopdienthoai.models.User;
import com.nor.shopdienthoai.utils.DataUtils;
import com.nor.shopdienthoai.utils.ShareDataUtls;

import java.util.ArrayList;

public class SplashActivity extends ActivityBase implements OnCompleteListener<AuthResult> {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initAct() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String email = ShareDataUtls.getInstance(SplashActivity.this).get(ShareDataUtls.Keys.EMAIL);
                if (!email.isEmpty()) {
                    doLogin(ShareDataUtls.getInstance(SplashActivity.this).get(ShareDataUtls.Keys.EMAIL),
                            ShareDataUtls.getInstance(SplashActivity.this).get(ShareDataUtls.Keys.PASSWORD));
                }else {
                    goToMain();
                }
            }
        }, 2000);
    }

    private void doLogin(String email, String password) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this);
    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        if (task.isSuccessful()) {
            DataUtils.getUser(new DataUtils.DataCallback<User>() {
                @Override
                public void onDataResult(ArrayList<User> data) {
                    App.getInstance().setUser(data.get(0));
                    goToMain();
                }
            }, ShareDataUtls.getInstance(this).get(ShareDataUtls.Keys.EMAIL));
        }else {
            goToMain();
        }
    }

    private void goToMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
