package com.nor.shopdienthoai.ui.item;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nor.shopdienthoai.R;

import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageHolder> {
    private ArrayList<String> data;
    private LayoutInflater inflater;
    private ImageItemListener listener;

    public ImageAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(ImageItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_image, parent, false);
        return new ImageHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageHolder holder, final int position) {
        holder.bindData(data.get(position));
        holder.imDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.remove(position);
                notifyItemRangeRemoved(position, 1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class ImageHolder extends RecyclerView.ViewHolder {
        private ImageView imItem;
        private ImageView imDelete;
        public ImageHolder(@NonNull View itemView) {
            super(itemView);
            imItem = itemView.findViewById(R.id.im_item);
            imDelete = itemView.findViewById(R.id.im_delete);
        }

        public void bindData(final String im) {
            Glide.with(imItem).load(im).into(imItem);
            if (listener != null) {
                imDelete.setVisibility(View.GONE);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onItemImageClick(im);
                    }
                });
            }
        }
    }

    public interface ImageItemListener {
        void onItemImageClick(String s);
    }
}
