package com.nor.shopdienthoai.ui.group;

import android.content.DialogInterface;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.FirebaseDatabase;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.ActivityBase;
import com.nor.shopdienthoai.models.Group;
import com.nor.shopdienthoai.utils.DataUtils;
import com.nor.shopdienthoai.utils.DialogUtls;

import java.util.ArrayList;

public class GroupActivity extends ActivityBase implements View.OnClickListener, GroupAdapter.GroupItemClickListener, DataUtils.DataCallback<Group> {

    private FloatingActionButton btnAdd;
    private RecyclerView lvGroup;
    private GroupAdapter adapter;

    @Override
    public boolean isShowBack() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_group;
    }

    @Override
    protected void initAct() {
        adapter = new GroupAdapter(getLayoutInflater(), this);
        btnAdd = findViewById(R.id.btn_add);
        lvGroup = findViewById(R.id.lv_group);
        lvGroup.setAdapter(adapter);
        btnAdd.setOnClickListener(this);

        DataUtils.getData(Group.class, this);
    }

    @Override
    public void onClick(View view) {
        startEdit(null);
    }

    private void startEdit(Group o) {
        startActivity(GroupEditActivity.getInstance(this, o));
    }

    @Override
    public void onItemGroupClick(Group group) {
        startEdit(group);
    }

    @Override
    public void onItemGroupLongClick(final Group group) {
        DialogUtls.showDeleteDialog(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FirebaseDatabase.getInstance().getReference(group.getRoot()).child(group.getId()+"").removeValue();
            }
        });
    }

    @Override
    public void onDataResult(ArrayList<Group> data) {
        adapter.setData(data);
    }
}
