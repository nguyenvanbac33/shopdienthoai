package com.nor.shopdienthoai.ui.home;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.FragmentBase;
import com.nor.shopdienthoai.models.Group;
import com.nor.shopdienthoai.utils.DataUtils;

import java.util.ArrayList;

public class HomeFragment extends FragmentBase implements DataUtils.DataCallback<Group>, View.OnClickListener {
    private ViewPager pager;
    private HomePagesAdapter adapter;
    private FloatingActionButton btnAdd;
    private TabLayout tab;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tab = findViewById(R.id.tab);
        btnAdd = findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
        pager = findViewById(R.id.pager);
        adapter = new HomePagesAdapter(getActivity().getSupportFragmentManager());
        pager.setAdapter(adapter);
        tab.setupWithViewPager(pager);
        DataUtils.getData(Group.class, this);
    }

    @Override
    public void onDataResult(ArrayList<Group> data) {
        if (adapter == null) {
            return;
        }
        adapter.setData(data);
    }

    @Override
    public void onClick(View view) {
        GroupItemPageFragment fm = (GroupItemPageFragment) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
        fm.addNewItem();
    }
}
