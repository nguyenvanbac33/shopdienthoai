package com.nor.shopdienthoai.ui.account;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.nor.shopdienthoai.App;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.models.User;
import com.nor.shopdienthoai.ui.MainActivity;
import com.nor.shopdienthoai.base.ActivityBase;
import com.nor.shopdienthoai.utils.Const;
import com.nor.shopdienthoai.utils.DataUtils;
import com.nor.shopdienthoai.utils.DialogUtls;
import com.nor.shopdienthoai.utils.ShareDataUtls;
import com.nor.shopdienthoai.utils.Validators;

import java.util.ArrayList;

public class LoginActivity extends ActivityBase implements View.OnClickListener, OnCompleteListener<AuthResult> {

    private Button btnRegister;
    private Button btnLogin;
    private EditText edtEmail;
    private EditText edtPassword;

    private FirebaseAuth auth = FirebaseAuth.getInstance();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initAct() {
        btnLogin = findViewById(R.id.btn_login);
        btnRegister = findViewById(R.id.btn_register);
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        btnRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                if (Validators.isEmpty(edtEmail, edtPassword)) {
                    return;
                }
                doLogin(edtEmail.getText().toString(), edtPassword.getText().toString());
                break;
            case R.id.btn_register:
                User user = new User();
                user.setUserType(Const.USER_GUEST);
                startActivity(RegisterActivity.newIntance(this, user));
                break;
        }
    }

    private void doLogin(String email, String password) {
        edtEmail.setText(email);
        edtPassword.setText(password);
        DialogUtls.showProgressDialog(this);
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this);
    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        if (task.isSuccessful()) {
            ShareDataUtls.getInstance(this).put(ShareDataUtls.Keys.EMAIL, edtEmail.getText().toString());
            ShareDataUtls.getInstance(this).put(ShareDataUtls.Keys.PASSWORD, edtPassword.getText().toString());
            DataUtils.getUser(new DataUtils.DataCallback<User>() {
                @Override
                public void onDataResult(ArrayList<User> data) {
                    DialogUtls.dismissProgress();
                    App.getInstance().setUser(data.get(0));
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, edtEmail.getText().toString());
        }else {
            DialogUtls.dismissProgress();
            Toast.makeText(this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
