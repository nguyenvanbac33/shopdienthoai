package com.nor.shopdienthoai.ui.home;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.FirebaseDatabase;
import com.nor.shopdienthoai.App;
import com.nor.shopdienthoai.models.Group;
import com.nor.shopdienthoai.models.Item;
import com.nor.shopdienthoai.ui.item.ItemActivity;
import com.nor.shopdienthoai.ui.item.ItemDetailActivity;
import com.nor.shopdienthoai.utils.Const;
import com.nor.shopdienthoai.utils.DataUtils;
import com.nor.shopdienthoai.utils.DialogUtls;

import java.util.ArrayList;

public class GroupItemPageFragment extends Fragment implements DataUtils.DataCallback<Item>, GroupItemAdapter.GroupItemListener {

    private RecyclerView lvItem;
    private GroupItemAdapter adapter;
    private Group group;

    public static GroupItemPageFragment newInstance(Group group) {
        GroupItemPageFragment fm = new GroupItemPageFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Group.class.getName(), group);
        fm.setArguments(bundle);
        return fm;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        lvItem = new RecyclerView(container.getContext());
        lvItem.setLayoutManager(new LinearLayoutManager(container.getContext()));
        return lvItem;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        group = (Group) getArguments().getSerializable(Group.class.getName());

        adapter = new GroupItemAdapter(getActivity().getLayoutInflater());
        adapter.setListener(this);
        lvItem.setAdapter(adapter);

        DataUtils.getItem(Item.class, group.getId(), this);
    }

    public void addNewItem() {
        Item item = new Item();
        item.setIdGroup(group.getId());
        startItemActivity(item);
    }

    private void startItemActivity(Item item) {
        startActivity(ItemActivity.newInstance(getContext(), item));
    }

    @Override
    public void onDataResult(ArrayList<Item> data) {
        adapter.setData(data);
    }

    @Override
    public void onGroupItemClick(Item item) {
        if (App.getInstance().getUser() != null && App.getInstance().getUser().getUserType() == Const.USER_ADMIN) {
            startItemActivity(item);
            return;
        }
        startActivity(ItemDetailActivity.newInstace(getContext(), item));
    }

    @Override
    public void onGroupItemLongClick(final Item item) {
        DialogUtls.showDeleteDialog(getContext(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FirebaseDatabase.getInstance().getReference(item.getRoot()).child(item.getId()+"").removeValue();
            }
        });
    }
}
