package com.nor.shopdienthoai.ui.cart;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.FragmentBase;
import com.nor.shopdienthoai.models.Cart;
import com.nor.shopdienthoai.ui.cart.cartdetail.CartDetailActivity;
import com.nor.shopdienthoai.utils.DataUtils;

import java.util.ArrayList;

public class CartFragment extends FragmentBase implements DataUtils.DataCallback<Cart>, CartAdapter.CartItemListener {

    private RecyclerView lvCart;
    private CartAdapter adapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_cart;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        lvCart = findViewById(R.id.lv_cart);
        adapter = new CartAdapter(getLayoutInflater());
        adapter.setListener(this);
        lvCart.setAdapter(adapter);
        DataUtils.getCart(this);
    }

    @Override
    public void onDataResult(ArrayList<Cart> data) {
        adapter.setData(data);
    }

    @Override
    public void onCartItemClick(Cart cart) {
        startActivity(CartDetailActivity.newInstance(getContext(), cart, false));
    }
}
