package com.nor.shopdienthoai.ui.group;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.models.Group;

import java.util.ArrayList;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.GroupHolder> {
    private ArrayList<Group> data;
    private LayoutInflater inflater;
    private GroupItemClickListener listener;

    public GroupAdapter(LayoutInflater inflater, GroupItemClickListener listener) {
        this.inflater = inflater;
        this.listener = listener;
    }

    public void setData(ArrayList<Group> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public GroupHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_group, parent, false);
        return new GroupHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupHolder holder, final int position) {
        holder.bindData(data.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemGroupClick(data.get(position));
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onItemGroupLongClick(data.get(position));
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class GroupHolder extends RecyclerView.ViewHolder{

        private TextView tvName;

        public GroupHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
        }

        public void bindData(Group item) {
            tvName.setText(item.getName());
        }
    }

    public interface GroupItemClickListener {
        void onItemGroupClick(Group group);
        void onItemGroupLongClick(Group group);
    }
}
