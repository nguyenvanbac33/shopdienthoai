package com.nor.shopdienthoai.ui.item;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nor.shopdienthoai.App;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.ActivityBase;
import com.nor.shopdienthoai.models.Cart;
import com.nor.shopdienthoai.models.CartDetail;
import com.nor.shopdienthoai.models.Item;
import com.nor.shopdienthoai.utils.ShareDataUtls;
import com.nor.shopdienthoai.utils.Validators;

public class ItemDetailActivity extends ActivityBase implements ImageAdapter.ImageItemListener, View.OnClickListener {

    private ImageView imItem;
    private RecyclerView lvImage;
    private AppCompatSpinner spinner;
    private TextView tvName;
    private TextView tvPrice;
    private WebView wvSummary;
    private EditText edtCount;
    private ImageView imCart;
    private ArrayAdapter<String> adapterColor;
    private ImageAdapter adapter;

    private Item item;

    public static Intent newInstace(Context context, Item item) {
        Intent intent = new Intent(context, ItemDetailActivity.class);
        intent.putExtra(Item.class.getName(), item);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_item_detail;
    }

    @Override
    protected void initAct() {
        item = (Item) getIntent().getSerializableExtra(Item.class.getName());
        imItem = findViewById(R.id.im_item);
        lvImage = findViewById(R.id.lv_image);
        tvName = findViewById(R.id.tv_name);
        tvPrice = findViewById(R.id.tv_price);
        wvSummary = findViewById(R.id.wv_summary);
        spinner = findViewById(R.id.sp_color);

        edtCount = findViewById(R.id.edt_count);
        imCart = findViewById(R.id.im_cart);
        imCart.setOnClickListener(this);

        adapterColor = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, item.getColors());
        spinner.setAdapter(adapterColor);
        tvName.setText(item.getName());
        tvPrice.setText(Validators.formatMoney(item.getPrice()));
        wvSummary.loadData(item.getSummary(), "text/html", "utf8");

        adapter = new ImageAdapter(getLayoutInflater());
        lvImage.setAdapter(adapter);
        adapter.setData(item.getImages());
        adapter.setListener(this);

        if (item.getImages().size() > 0) {
            onItemImageClick(item.getImages().get(0));
        }
    }

    @Override
    public boolean isShowBack() {
        return true;
    }

    @Override
    public void onItemImageClick(String s) {
        Glide.with(imItem).load(s).into(imItem);
    }

    @Override
    public void onClick(View view) {
        if (Validators.isEmpty(edtCount)) return;
        int count = Integer.parseInt(edtCount.getText().toString());
        CartDetail detail = new CartDetail();
        detail.setCount(count);
        detail.setItem(item);
        detail.setColor(spinner.getSelectedItem().toString());
        Cart c = App.getInstance().getCarts().getValue();
        c.getDetails().add(detail);
        c.setEmail(ShareDataUtls.getInstance(this).get(ShareDataUtls.Keys.EMAIL));
        App.getInstance().getCarts().postValue(c);
        Toast.makeText(this, "Add to cart successfully", Toast.LENGTH_SHORT).show();
    }
}
