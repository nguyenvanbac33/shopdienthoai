package com.nor.shopdienthoai.ui.cart;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.models.Cart;
import com.nor.shopdienthoai.utils.Validators;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartHolder> {

    private ArrayList<Cart> data;
    private LayoutInflater inflater;
    private CartItemListener listener;

    public CartAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setData(ArrayList<Cart> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(CartItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public CartHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_cart, parent, false);
        return new CartHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartHolder holder, final int position) {
        holder.bindData(data.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCartItemClick(data.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class CartHolder extends RecyclerView.ViewHolder {

        private TextView tvDate;
        private TextView tvName;
        private TextView tvPhone;
        private TextView tvAddress;
        private TextView tvStatus;

        public CartHolder(@NonNull View itemView) {
            super(itemView);
            tvAddress = itemView.findViewById(R.id.tv_address);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPhone = itemView.findViewById(R.id.tv_phone);
            tvStatus = itemView.findViewById(R.id.tv_status);
        }

        public void bindData(Cart cart) {
            tvAddress.setText(cart.getUser().getAddress());
            tvName.setText(cart.getUser().getName());
            tvPhone.setText(cart.getUser().getPhone());
            tvDate.setText(Validators.formatDate(cart.getId()));
            tvStatus.setText(Validators.getStatus(cart.getStatus()));
        }
    }

    public interface CartItemListener {
        void onCartItemClick(Cart cart);
    }
}
