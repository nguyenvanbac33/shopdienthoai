package com.nor.shopdienthoai.ui;

import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.nor.shopdienthoai.App;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.ActivityBase;
import com.nor.shopdienthoai.base.FragmentBase;
import com.nor.shopdienthoai.ui.profile.ProfileFragment;
import com.nor.shopdienthoai.ui.cart.CartFragment;
import com.nor.shopdienthoai.ui.home.HomeFragment;
import com.nor.shopdienthoai.utils.Const;


public class MainActivity extends ActivityBase implements BottomNavigationView.OnNavigationItemSelectedListener {

    private ProfileFragment fmAccount = new ProfileFragment();
    private CartFragment fmCart = new CartFragment();
    private HomeFragment fmHome = new HomeFragment();

    private FragmentBase[] fms = {
            fmAccount, fmHome, fmCart
    };

    private BottomNavigationView nav;
    private DrawerLayout drawer;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initAct() {
        nav = findViewById(R.id.nav_view);
        nav.setOnNavigationItemSelectedListener(this);
        drawer = findViewById(R.id.drawer);

        initFragment();
        showFragment(fmHome);

        if (App.getInstance().getUser() == null
                || App.getInstance().getUser().getUserType() == Const.USER_GUEST
                || App.getInstance().getUser().getUserType() == Const.USER_EMPLOYEE) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    private void initFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        for (FragmentBase fm: fms) {
            transaction.add(R.id.nav_host_fragment, fm);
        }
        transaction.commitAllowingStateLoss();
    }

    private void showFragment(FragmentBase fmShow) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        for (FragmentBase fm: fms) {
            transaction.hide(fm);
        }
        transaction.show(fmShow);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                showFragment(fmHome);
                break;
            case R.id.nav_account:
                showFragment(fmAccount);
                break;
            case R.id.nav_cart:
                showFragment(fmCart);
                break;
        }
        return true;
    }

    @Override
    public boolean isShowBack() {
        if (App.getInstance().getUser() == null
                || App.getInstance().getUser().getUserType() == Const.USER_GUEST
                || App.getInstance().getUser().getUserType() == Const.USER_EMPLOYEE) {
            return false;
        }
        return true;
    }

    public void closeDrawer() {
        drawer.closeDrawers();
    }

    public DrawerLayout getDrawer() {
        return drawer;
    }
}
