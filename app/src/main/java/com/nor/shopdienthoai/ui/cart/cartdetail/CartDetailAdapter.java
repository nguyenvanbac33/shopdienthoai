package com.nor.shopdienthoai.ui.cart.cartdetail;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nor.shopdienthoai.App;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.models.CartDetail;
import com.nor.shopdienthoai.utils.Validators;

import java.util.ArrayList;

public class CartDetailAdapter extends RecyclerView.Adapter<CartDetailAdapter.CartHolder> {

    private ArrayList<CartDetail> data;
    private LayoutInflater inflater;
    private Boolean isOrder;

    public CartDetailAdapter(LayoutInflater inflater, boolean isOrder) {
        this.inflater = inflater;
        this.isOrder = isOrder;
    }

    public void setData(ArrayList<CartDetail> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CartHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_cart_detail, parent, false);
        return new CartHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CartHolder holder, final int position) {
        final CartDetail detail = data.get(position);
        holder.bindData(detail);
        holder.imAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detail.setCount(detail.getCount()+ 1);
                notifyItemChanged(position);
            }
        });
        holder.imSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detail.getCount() == 1) {
                    return;
                }
                detail.setCount(detail.getCount() - 1);
                notifyItemChanged(position);
            }
        });
        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.remove(position);
                App.getInstance().getCarts().postValue(App.getInstance().getCarts().getValue());
                notifyItemRangeRemoved(position, 1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class CartHolder extends RecyclerView.ViewHolder {
        private ImageView imItem;
        private TextView tvName;
        private TextView tvPrice;
        private TextView tvTotalPrice;
        private ImageView imSub;
        private ImageView imAdd;
        private TextView tvCount;
        private TextView tvDelete;
        private TextView tvColor;

        public CartHolder(@NonNull View itemView) {
            super(itemView);
            imAdd = itemView.findViewById(R.id.im_add);
            imItem = itemView.findViewById(R.id.im_item);
            imSub = itemView.findViewById(R.id.im_sub);
            tvCount = itemView.findViewById(R.id.tv_count);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPrice = itemView.findViewById(R.id.tv_price);
            tvTotalPrice = itemView.findViewById(R.id.tv_total_price);
            tvDelete = itemView.findViewById(R.id.tv_delete);
            tvColor = itemView.findViewById(R.id.tv_color);
            if (!isOrder) {
                itemView.findViewById(R.id.control).setVisibility(View.GONE);
            }
        }

        public void bindData(final CartDetail detail) {
            Glide.with(imItem).load(detail.getItem().getImages().get(0)).into(imItem);
            tvName.setText(detail.getItem().getName());
            tvPrice.setText(Validators.formatMoney(detail.getItem().getPrice()));
            tvCount.setText(detail.getCount() + "");
            String total = Validators.formatMoney(detail.getItem().getPrice() * detail.getCount());
            if (!isOrder) {
                total =  Validators.formatMoney(detail.getItem().getPrice()) + " X " + detail.getCount() +" = " + total;
            }
            tvTotalPrice.setText(total);
            tvColor.setText(detail.getColor());
        }
    }
}
