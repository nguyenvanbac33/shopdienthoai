package com.nor.shopdienthoai.ui.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.models.Item;
import com.nor.shopdienthoai.utils.Validators;

import java.util.ArrayList;

public class GroupItemAdapter extends RecyclerView.Adapter<GroupItemAdapter.GroupItemHolder> {
    private ArrayList<Item> data;
    private LayoutInflater inflater;
    private GroupItemListener listener;

    public GroupItemAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setListener(GroupItemListener listener) {
        this.listener = listener;
    }

    public void setData(ArrayList<Item> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public GroupItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_item, parent, false);
        return new GroupItemHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupItemHolder holder, final int position) {
        holder.bindData(data.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onGroupItemClick(data.get(position));
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onGroupItemLongClick(data.get(position));
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class GroupItemHolder extends RecyclerView.ViewHolder {
        private ImageView imItem;
        private TextView tvName;
        private TextView tvPrice;

        public GroupItemHolder(@NonNull View itemView) {
            super(itemView);
            imItem = itemView.findViewById(R.id.im_item);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPrice = itemView.findViewById(R.id.tv_price);
        }

        public void bindData(Item item) {
            tvName.setText(item.getName());
            tvPrice.setText(Validators.formatMoney(item.getPrice()));
            Glide.with(imItem).load(item.getImages().get(0)).into(imItem);
        }
    }

    public interface GroupItemListener {
        void onGroupItemClick(Item item);
        void onGroupItemLongClick(Item item);
    }
}
