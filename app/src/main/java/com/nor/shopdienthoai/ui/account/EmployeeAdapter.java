package com.nor.shopdienthoai.ui.account;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.models.User;

import java.util.ArrayList;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeHolder> {
    private ArrayList<User> data;
    private LayoutInflater inflater;
    private EmployeeItemListener listener;

    public EmployeeAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setData(ArrayList<User> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(EmployeeItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public EmployeeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_employee, parent, false);
        return new EmployeeHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeHolder holder, final int position) {
        holder.bindData(data.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.employeeItemClick(data.get(position));
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.employeeItemLongClick(data.get(position));
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class EmployeeHolder extends RecyclerView.ViewHolder {

        private ImageView imAvatar;
        private TextView tvName;
        private TextView tvAddress;
        private TextView tvPhone;
        private TextView tvEmail;

        public EmployeeHolder(@NonNull View itemView) {
            super(itemView);
            imAvatar = itemView.findViewById(R.id.im_avatar);
            tvAddress = itemView.findViewById(R.id.tv_address);
            tvName = itemView.findViewById(R.id.tv_name);
            tvPhone = itemView.findViewById(R.id.tv_phone);
            tvEmail = itemView.findViewById(R.id.tv_email);
        }

        public void bindData(User user) {
            tvName.setText(user.getName());
            tvPhone.setText(user.getPhone());
            tvEmail.setText(user.getEmail());
            tvAddress.setText(user.getAddress());
            Glide.with(imAvatar).load(user.getAvatar()).placeholder(R.drawable.ic_person).error(R.drawable.ic_person).into(imAvatar);
        }
    }

    public interface EmployeeItemListener {
        void employeeItemClick(User user);
        void employeeItemLongClick(User user);
    }
}
