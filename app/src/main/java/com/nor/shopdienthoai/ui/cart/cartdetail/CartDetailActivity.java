package com.nor.shopdienthoai.ui.cart.cartdetail;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nor.shopdienthoai.App;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.ActivityBase;
import com.nor.shopdienthoai.models.Cart;
import com.nor.shopdienthoai.models.CartDetail;
import com.nor.shopdienthoai.ui.account.LoginActivity;
import com.nor.shopdienthoai.utils.Const;
import com.nor.shopdienthoai.utils.DialogUtls;
import com.nor.shopdienthoai.utils.Validators;

public class CartDetailActivity extends ActivityBase implements View.OnClickListener {

    private CartDetailAdapter adapter;
    private RecyclerView lvCart;
    private Button btnOrder;
    private Cart cart;
    private boolean isOrder;
    private Button btnCancel;

    public static final String EXTRA_ORDER = "extra.order";

    public static Intent newInstance(Context context, Cart cart, boolean isOrder) {
        Intent intent = new Intent(context, CartDetailActivity.class);
        intent.putExtra(EXTRA_ORDER, isOrder);
        intent.putExtra(Cart.class.getName(), cart);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_cart_detail;
    }

    @Override
    protected void initAct() {
        cart = (Cart) getIntent().getSerializableExtra(Cart.class.getName());
        isOrder = getIntent().getBooleanExtra(EXTRA_ORDER, false);
        if (isOrder) {
            cart = App.getInstance().getCarts().getValue();
        }
        lvCart = findViewById(R.id.lv_cart);
        btnOrder = findViewById(R.id.btn_order);
        btnOrder.setOnClickListener(this);
        btnCancel = findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        adapter = new CartDetailAdapter(getLayoutInflater(), isOrder);
        adapter.setData(cart.getDetails());
        int sum = 0;
        for (CartDetail d : cart.getDetails()) {
            sum += d.getCount() * d.getItem().getPrice();
        }
        ((TextView) findViewById(R.id.tv_sum)).setText("Tổng tiền: " + Validators.formatMoney(sum));
        lvCart.setAdapter(adapter);
        if (!isOrder) {
            if (cart.getStatus() < 0 || cart.getStatus() >= 3 || App.getInstance().getUser().getUserType() == Const.USER_GUEST) {
                btnOrder.setVisibility(View.GONE);
            }else {
                btnOrder.setText(Validators.getStatusConfirm(cart.getStatus()));
            }
            if (cart.getStatus() < 0 || cart.getStatus() >= 3) {
                btnCancel.setVisibility(View.GONE);
            }else {
                btnCancel.setVisibility(View.VISIBLE);
            }
        }else {
            btnCancel.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        if (App.getInstance().getUser() == null) {
            new AlertDialog.Builder(this)
                    .setMessage("You need to login before order")
                    .setTitle("Need login")
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(CartDetailActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .show();
            return;
        }
        switch (view.getId()) {
            case R.id.btn_order:
                if (!isOrder) {
                    cart.setStatus(cart.getStatus() + 1);
                }else {
                    cart.setEmail(App.getInstance().getUser().getEmail());
                }
                break;
            case R.id.btn_cancel:
                cart.setStatus(-1);
                break;
        }
        pushData(cart);
    }

    @Override
    protected void onPushSuccess() {
        super.onPushSuccess();
        if (isOrder) {
            App.getInstance().getCarts().postValue(new Cart());
        }
        finish();
    }

    @Override
    public boolean isShowBack() {
        return true;
    }
}
