package com.nor.shopdienthoai.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.navigation.NavigationView;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.FragmentBase;
import com.nor.shopdienthoai.ui.MainActivity;
import com.nor.shopdienthoai.ui.account.EmployeeActivity;
import com.nor.shopdienthoai.ui.group.GroupActivity;

public class MenuFragment extends FragmentBase implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView nav;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_menu;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        nav = findViewById(R.id.nav_menu);
        nav.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        ((MainActivity) getActivity()).closeDrawer();
        switch (item.getItemId()) {
            case R.id.menu_group:
                startActivity(new Intent(getContext(), GroupActivity.class));
                break;
            case R.id.menu_employee:
                startActivity(new Intent(getContext(), EmployeeActivity.class));
                break;
        }
        return true;
    }
}
