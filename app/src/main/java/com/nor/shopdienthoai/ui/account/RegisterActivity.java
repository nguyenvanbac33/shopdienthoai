package com.nor.shopdienthoai.ui.account;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.ActivityBase;
import com.nor.shopdienthoai.models.User;
import com.nor.shopdienthoai.utils.DialogUtls;
import com.nor.shopdienthoai.utils.Validators;

public class RegisterActivity extends ActivityBase implements View.OnClickListener, OnCompleteListener<AuthResult> {

    private EditText edtName;
    private EditText edtEmail;
    private EditText edtPassword;
    private EditText edtPhone;
    private EditText edtAddress;
    private Button btnRegister;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private User user;

    public static Intent newIntance(Context context, User user) {
        Intent intent = new Intent(context, RegisterActivity.class);
        intent.putExtra(User.class.getName(), user);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    protected void initAct() {
        user = (User) getIntent().getSerializableExtra(User.class.getName());
        edtAddress = findViewById(R.id.edt_address);
        edtEmail = findViewById(R.id.edt_email);
        edtName = findViewById(R.id.edt_name);
        edtPassword = findViewById(R.id.edt_password);
        edtPhone = findViewById(R.id.edt_sdt);
        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(this);

        if (user.getName() != null) {
            edtAddress.setText(user.getAddress());
            edtEmail.setText(user.getEmail());
            edtName.setText(user.getName());
            edtPassword.setText(user.getPassword());
            edtPhone.setText(user.getPhone());
            edtEmail.setEnabled(false);
        }
    }

    @Override
    public void onClick(View view) {
        if (Validators.isEmpty(edtAddress, edtEmail, edtName, edtPassword, edtPhone)) {
            return;
        }
        user.setAddress(edtAddress.getText().toString());
        user.setEmail(edtEmail.getText().toString());
        user.setName(edtName.getText().toString());
        user.setPassword(edtPassword.getText().toString());
        user.setPhone(edtPhone.getText().toString());
        if (edtEmail.isEnabled() == true) {
            DialogUtls.showProgressDialog(this);
            mAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                    .addOnCompleteListener(this);
        }else {
            mAuth.getCurrentUser().updatePassword(user.getPassword());
            pushData(user);
        }
    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        DialogUtls.dismissProgress();
        if (task.isSuccessful()) {
            pushData(user);
        }else {
            Toast.makeText(this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPushSuccess() {
        super.onPushSuccess();
        finish();
    }

    @Override
    public boolean isShowBack() {
        return true;
    }
}
