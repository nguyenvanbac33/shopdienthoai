package com.nor.shopdienthoai.ui.item;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.ActivityBase;
import com.nor.shopdienthoai.models.Item;
import com.nor.shopdienthoai.utils.StorageUtils;
import com.nor.shopdienthoai.utils.Validators;

public class ItemActivity extends ActivityBase implements View.OnClickListener, StorageUtils.UploadCallback {

    private EditText edtName;
    private EditText edtPrice;
    private EditText edtSummary;
    private Button btnUpdate;
    private ImageView imItem;
    private RecyclerView lvImage;
    private RecyclerView lvColor;

    private EditText edtColor;
    private Button btnAdd;

    private ColorAdapter adapterColor;
    private ImageAdapter adapter;

    private Item item;
    private final int REQUEST_IMAGE = 1;

    public static Intent newInstance(Context context, Item item) {
        Intent intent = new Intent(context, ItemActivity.class);
        intent.putExtra(Item.class.getName(), item);
        return intent;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_item;
    }

    @Override
    protected void initAct() {
        item = (Item) getIntent().getSerializableExtra(Item.class.getName());
        edtName = findViewById(R.id.edt_name);
        edtPrice = findViewById(R.id.edt_price);
        edtSummary = findViewById(R.id.edt_desc);
        btnUpdate = findViewById(R.id.btn_update);
        imItem = findViewById(R.id.im_item);
        lvImage = findViewById(R.id.lv_im);
        lvColor = findViewById(R.id.lv_color);

        edtColor = findViewById(R.id.edt_color);
        btnAdd = findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);

        adapterColor = new ColorAdapter(getLayoutInflater());
        adapter = new ImageAdapter(getLayoutInflater());
        lvImage.setAdapter(adapter);
        lvColor.setAdapter(adapterColor);

        if (!TextUtils.isEmpty(item.getName())) {
            edtName.setText(item.getName());
            edtSummary.setText(item.getSummary());
            edtPrice.setText(item.getPrice()+"");
            adapter.setData(item.getImages());
            adapterColor.setData(item.getColors());
        }

        imItem.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.im_item:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Pick image"), REQUEST_IMAGE);
                break;
            case R.id.btn_update:
                if (Validators.isEmpty(edtName, edtPrice, edtSummary)) return;
                item.setName(edtName.getText().toString());
                item.setPrice(Integer.parseInt(edtPrice.getText().toString()));
                item.setSummary(edtSummary.getText().toString());
                pushData(item);
                break;
            case R.id.btn_add:
                if (Validators.isEmpty(edtColor)) return;
                String name = edtColor.getText().toString();
                item.getColors().add(name);
                adapterColor.setData(item.getColors());
                edtColor.setText("");
                break;
        }
    }

    @Override
    public boolean isShowBack() {
        return true;
    }

    @Override
    protected void onPushSuccess() {
        super.onPushSuccess();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            StorageUtils.uploadImage(this, uri, this);
        }
    }

    @Override
    public void onUploadSuccess(String path) {
        item.getImages().add(path);
        adapter.setData(item.getImages());
    }
}
