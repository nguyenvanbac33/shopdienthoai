package com.nor.shopdienthoai.ui.account;

import android.content.DialogInterface;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.FirebaseDatabase;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.ActivityBase;
import com.nor.shopdienthoai.models.User;
import com.nor.shopdienthoai.utils.Const;
import com.nor.shopdienthoai.utils.DataUtils;
import com.nor.shopdienthoai.utils.DialogUtls;

import java.util.ArrayList;

public class EmployeeActivity extends ActivityBase implements View.OnClickListener, EmployeeAdapter.EmployeeItemListener, DataUtils.DataCallback<User> {
    private RecyclerView lvEmployee;
    private FloatingActionButton btnAdd;
    private EmployeeAdapter adapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_employee;
    }

    @Override
    protected void initAct() {
        lvEmployee = findViewById(R.id.lv_employee);
        btnAdd = findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);

        adapter = new EmployeeAdapter(getLayoutInflater());
        adapter.setListener(this);
        lvEmployee.setAdapter(adapter);
        DataUtils.getEmployee(this);
    }

    @Override
    public void onClick(View view) {
        User user = new User();
        user.setUserType(Const.USER_EMPLOYEE);
        startActivity(RegisterActivity.newIntance(this, user));
    }

    @Override
    public void employeeItemClick(User user) {
        startActivity(RegisterActivity.newIntance(this, user));
    }

    @Override
    public void employeeItemLongClick(final User user) {
        DialogUtls.showDeleteDialog(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FirebaseDatabase.getInstance().getReference(user.getRoot()).child(user.getId()+"").removeValue();
            }
        });
    }

    @Override
    public void onDataResult(ArrayList<User> data) {
        adapter.setData(data);
    }
}
