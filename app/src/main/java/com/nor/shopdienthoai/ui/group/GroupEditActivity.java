package com.nor.shopdienthoai.ui.group;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.ActivityBase;
import com.nor.shopdienthoai.models.Group;
import com.nor.shopdienthoai.utils.Validators;

public class GroupEditActivity extends ActivityBase implements View.OnClickListener {

    private EditText edtName;
    private Button btnUpdate;
    private Group group;

    public static Intent getInstance(Context context, Group group) {
        Intent intent = new Intent(context, GroupEditActivity.class);
        intent.putExtra(Group.class.getName(), group);
        return intent;
    }

    @Override
    public boolean isShowBack() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edt_group;
    }

    @Override
    protected void initAct() {
        btnUpdate = findViewById(R.id.btn_update);
        edtName = findViewById(R.id.edt_name);
        if (getIntent().getSerializableExtra(Group.class.getName()) != null) {
            group = (Group) getIntent().getSerializableExtra(Group.class.getName());
            edtName.setText(group.getName());
        }else {
            group = new Group();
        }

        btnUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (Validators.isEmpty(edtName)) return;
        group.setName(edtName.getText().toString());
        pushData(group);
    }

    @Override
    protected void onPushSuccess() {
        super.onPushSuccess();
        finish();
    }
}
