package com.nor.shopdienthoai.models;

public class Group extends BaseModel{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getRoot() {
        return getClass().getSimpleName();
    }
}
