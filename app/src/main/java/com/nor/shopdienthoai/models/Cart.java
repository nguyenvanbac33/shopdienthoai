package com.nor.shopdienthoai.models;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;

public class Cart extends BaseModel {
    private ArrayList<CartDetail> details = new ArrayList<>();
    private int status = 0;
    private String email;
    @Exclude
    private User user;

    public ArrayList<CartDetail> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<CartDetail> details) {
        this.details = details;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String getRoot() {
        return getClass().getSimpleName();
    }
}
