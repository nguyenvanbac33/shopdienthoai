package com.nor.shopdienthoai.models;

public class CartDetail extends BaseModel {
    private Item item;
    private int count;
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String getRoot() {
        return getClass().getSimpleName();
    }
}
