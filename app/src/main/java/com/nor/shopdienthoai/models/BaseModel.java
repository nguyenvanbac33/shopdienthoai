package com.nor.shopdienthoai.models;

import java.io.Serializable;

public abstract class BaseModel implements Serializable {
    private long id = System.currentTimeMillis();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public abstract String getRoot();
}
