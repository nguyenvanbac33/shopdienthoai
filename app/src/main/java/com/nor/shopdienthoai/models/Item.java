package com.nor.shopdienthoai.models;

import java.util.ArrayList;

public class Item extends BaseModel{
    private String name;
    private int price;
    private String summary;
    private ArrayList<String> images = new ArrayList<>();
    private ArrayList<String> colors = new ArrayList<>();
    private long idGroup;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public long getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(long idGroup) {
        this.idGroup = idGroup;
    }

    public ArrayList<String> getColors() {
        return colors;
    }

    public void setColors(ArrayList<String> colors) {
        this.colors = colors;
    }

    @Override
    public String getRoot() {
        return getClass().getSimpleName();
    }
}
