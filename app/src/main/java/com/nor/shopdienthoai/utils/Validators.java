package com.nor.shopdienthoai.utils;

import android.widget.EditText;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

public class Validators {
    public static boolean isEmpty(EditText ... edts) {
        boolean isEmpty = false;
        for (EditText edt: edts) {
            if (edt.getText().toString().isEmpty()) {
                edt.setError("Input empty");
                isEmpty = true;
            }
        }
        return  isEmpty;
    }

    public static String formatMoney(int money) {
        NumberFormat formatter = new DecimalFormat("#,###");
        return formatter.format(money);
    }

    public static String formatDate(long date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    public static String getStatus(int status) {
        switch (status) {
            case 0:
                return "Order";
            case 1:
                return "Confirmed";
            case 2:
                return "Transfer";
            case 3:
                return "Paid";
            default:
                return "Cancelled";
        }
    }

    public static String getStatusConfirm(int status) {
        switch (status) {
            case 0:
                return "Confirm";
            case 1:
                return "Transfer";
            case 2:
                return "Paid";
            default:
                return "";
        }
    }
}
