package com.nor.shopdienthoai.utils;

public interface Const {
    int USER_GUEST = 0;
    int USER_ADMIN = 1;
    int USER_EMPLOYEE = 2;
}
