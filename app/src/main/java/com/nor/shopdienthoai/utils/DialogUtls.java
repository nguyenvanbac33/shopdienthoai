package com.nor.shopdienthoai.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

public class DialogUtls {
    private static ProgressDialog dialog;

    public static void showProgressDialog(Context context) {
        dismissProgress();
        dialog = new ProgressDialog(context);
        dialog.setMessage("Loading...");
        dialog.show();
    }

    public static void dismissProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public static void showDeleteDialog(Context context, final DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(context)
                .setTitle("Delete")
                .setMessage("Do you want to delete")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        listener.onClick(dialogInterface, i);
                    }
                }).show();
    }
}
