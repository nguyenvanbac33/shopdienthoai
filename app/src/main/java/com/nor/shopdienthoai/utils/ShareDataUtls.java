package com.nor.shopdienthoai.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class ShareDataUtls {
    private static ShareDataUtls instance;
    private SharedPreferences preferences;

    public enum  Keys {
        EMAIL, PASSWORD
    }

    private ShareDataUtls(Context context) {
        preferences  = context.getSharedPreferences(getClass().getSimpleName(), Context.MODE_PRIVATE);
    }

    public static ShareDataUtls getInstance(Context context) {
        if (instance == null) instance = new ShareDataUtls(context);
        return instance;
    }

    public void put(Keys key, String data) {
        preferences.edit().putString(key.toString(), data).commit();
    }

    public String get(Keys key) {
        return preferences.getString(key.toString(), "");
    }
}
