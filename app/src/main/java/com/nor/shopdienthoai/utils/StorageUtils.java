package com.nor.shopdienthoai.utils;

import android.content.Context;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class StorageUtils {
    private static StorageReference storageRef = FirebaseStorage.getInstance().getReference();

    public static void uploadImage(final Context context, Uri uri, final UploadCallback callback) {
        DialogUtls.showProgressDialog(context);
        final StorageReference riversRef = storageRef.child("images/"+uri.getLastPathSegment());
        riversRef.putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    task.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            return riversRef.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            DialogUtls.dismissProgress();
                            if (task.isSuccessful()) {
                                Uri uri = task.getResult();
                                callback.onUploadSuccess(uri.toString());
                            }
                        }
                    });
                }else {
                    DialogUtls.dismissProgress();
                    Toast.makeText(context, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public interface UploadCallback {
        void onUploadSuccess(String path);
    }
}
