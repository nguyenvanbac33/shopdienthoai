package com.nor.shopdienthoai.utils;

import android.os.Handler;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nor.shopdienthoai.App;
import com.nor.shopdienthoai.models.BaseModel;
import com.nor.shopdienthoai.models.Cart;
import com.nor.shopdienthoai.models.Item;
import com.nor.shopdienthoai.models.User;

import java.util.ArrayList;

public class DataUtils {
    public static <T extends BaseModel> void getData(final Class<T> clz, final DataCallback<T> callback) {
        try {
            T t = clz.newInstance();
            getData(FirebaseDatabase.getInstance().getReference(t.getRoot()), clz, callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static <T extends BaseModel> void getItem(final Class<T> clz, long groupId, final DataCallback<T> callback) {
        try {
            T t = clz.newInstance();
            Query reference = FirebaseDatabase.getInstance().getReference(t.getRoot()).orderByChild("idGroup").equalTo(groupId);
            getData(reference, clz, callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getEmployee(DataCallback<User> callback) {
        try {
            Query reference = FirebaseDatabase.getInstance().getReference(new User().getRoot()).orderByChild("userType").equalTo(Const.USER_EMPLOYEE);
            getData(reference, User.class, callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getUser(DataCallback<User> callback, String email) {
        try {
            Query reference = FirebaseDatabase.getInstance().getReference(new User().getRoot()).orderByChild("email").equalTo(email);
            getData(reference, User.class, callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getCart(final DataCallback<Cart> callback) {
        if (App.getInstance().getUser() == null) return;
        Query query;
        if (App.getInstance().getUser().getUserType() == Const.USER_GUEST) {
            query = FirebaseDatabase.getInstance().getReference(new Cart().getRoot()).orderByChild("email").equalTo(App.getInstance().getUser().getEmail());
        }else {
            query = FirebaseDatabase.getInstance().getReference(new Cart().getRoot());
        }
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final ArrayList<Cart> carts = new ArrayList<>();
                for (DataSnapshot sn: dataSnapshot.getChildren()) {
                    final Cart c = sn.getValue(Cart.class);
                    FirebaseDatabase.getInstance().getReference(new User().getRoot()).orderByChild("email").equalTo(c.getEmail())
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    for (DataSnapshot sn: dataSnapshot.getChildren()) {
                                        User user = sn.getValue(User.class);
                                        c.setUser(user);
                                        carts.add(c);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        callback.onDataResult(carts);
                    }
                }, 3000);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private static <T extends BaseModel> void getData(Query reference, final Class<T> clz, final DataCallback<T> callback) {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<T> data = new ArrayList<>();
                for (DataSnapshot sn: dataSnapshot.getChildren()) {
                    data.add(sn.getValue(clz));
                }
                callback.onDataResult(data);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public interface DataCallback<T extends BaseModel> {
        void onDataResult(ArrayList<T> data);
    }
}
