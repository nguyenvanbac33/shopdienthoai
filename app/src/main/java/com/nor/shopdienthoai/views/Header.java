package com.nor.shopdienthoai.views;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import com.nor.shopdienthoai.App;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.base.ActivityBase;
import com.nor.shopdienthoai.models.Cart;
import com.nor.shopdienthoai.ui.MainActivity;
import com.nor.shopdienthoai.ui.SplashActivity;
import com.nor.shopdienthoai.ui.account.LoginActivity;
import com.nor.shopdienthoai.ui.account.RegisterActivity;
import com.nor.shopdienthoai.ui.cart.cartdetail.CartDetailActivity;

public class Header extends FrameLayout implements View.OnClickListener {
    private ImageView imBack;
    private TextView tvTitle;
    private ImageView imCart;
    private TextView tvCount;

    public Header(@NonNull Context context) {
        super(context);
        init();
    }

    public Header(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Header(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public Header(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.ui_header, this, true);
        imBack = findViewById(R.id.im_back);
        imCart = findViewById(R.id.im_cart_list);
        tvTitle = findViewById(R.id.tv_title);
        tvCount = findViewById(R.id.tv_count);

        App.getInstance().getCarts().observe((AppCompatActivity)getContext(), new Observer<Cart>() {
            @Override
            public void onChanged(Cart cart) {
                tvCount.setText(cart.getDetails().size() + "");
            }
        });

        tvTitle.setText(((AppCompatActivity) getContext()).getTitle());
        imBack.setOnClickListener(this);
        imCart.setOnClickListener(this);

        imBack.setVisibility(((ActivityBase) getContext()).isShowBack() ? VISIBLE: GONE);
        if (getContext() instanceof LoginActivity || getContext() instanceof SplashActivity) {
            setVisibility(GONE);
        }else if (getContext() instanceof RegisterActivity) {
            imCart.setVisibility(GONE);
            tvCount.setVisibility(GONE);
        }

        if (getContext() instanceof MainActivity) {
            imBack.setImageResource(R.drawable.ic_menu);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.im_back:
                if (getContext() instanceof MainActivity) {
                    MainActivity act = (MainActivity) getContext();
                    if (act.getDrawer().isDrawerOpen(Gravity.LEFT)) {
                      act.closeDrawer();
                    } else {
                        act.getDrawer().openDrawer(Gravity.LEFT);
                    }
                    return;
                }
                ((AppCompatActivity) getContext()).finish();
                break;
            case R.id.im_cart_list:
                if (getContext() instanceof CartDetailActivity) {
                    return;
                }
                if (App.getInstance().getCarts().getValue().getDetails().isEmpty()) {
                    Toast.makeText(getContext(), "No item", Toast.LENGTH_SHORT).show();
                    return;
                }
                Cart cart = App.getInstance().getCarts().getValue();
                getContext().startActivity(CartDetailActivity.newInstance(getContext(), cart, true));
                break;
        }
    }
}
