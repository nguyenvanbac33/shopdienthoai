package com.nor.shopdienthoai.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nor.shopdienthoai.App;
import com.nor.shopdienthoai.R;
import com.nor.shopdienthoai.utils.Const;

public class AddButton extends FloatingActionButton {
    public AddButton(@NonNull Context context) {
        super(context);
        init();
    }

    public AddButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AddButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setImageResource(R.drawable.ic_add);
        setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        if (App.getInstance().getUser() != null && App.getInstance().getUser().getUserType() == Const.USER_ADMIN) {
            setVisibility(VISIBLE);
        }else {
            setVisibility(GONE);
        }
    }
}
